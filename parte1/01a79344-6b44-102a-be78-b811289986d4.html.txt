[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                          | Ubicación física del documento original:  |
| 1574 Abril, 27                                                                                                                                                                                                                                                  | CAJA A-7, EXP. 13, 2 FF.                  |
|                                                                                                                                                                                                                                                                 | Archivo Histórico del Municipio de Colima |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 250. 1574. Abril, 27.                                                                                                                                                                                                                                           |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| Información y provança hecha a petición de Diego Morán y su hijo Gonzalo, sobre lo que les acusa Juan de Iniesta, de que le pusieron fuego a su casa.                                                                                                           |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| Siendo escribano Baltasar de Alcalá, Julián de Frías en nombre de Gonzalo Morán presentó un escrito ante el alcalde ordinario de la Villa de Colima Luis de Grijalba, donde decía:                                                                              |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| "En nombre de Diego Morán e Gonzalo Moreno e, por virtud del poder que de él tengo, de que hago presentación, parezco ante V.m. y hago presentación de esta carta receptoría e interrogatorio y pido la mande V.m. guardar e cumplir como en ella se contiene". |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| Pide, además, que los testigos que va a presentar, sean examinados a tenor de las preguntas del mencionado interrogatorio.^([1])                                                                                                                                |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| Caja A–7, exp. 13, 2 ff.                                                                                                                                                                                                                                        |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| ---------------                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                 |                                           |
| (1) Expediente que sólo contiene la portadilla y el escrito reseñado de Julián de Frías.                                                                                                                                                                        |                                           |
+-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA7/EXP13/001V.jpg)

Archivo no encontrado (CAJAS/CAJA7/EXP13/002V.jpg)

[]
1F

[]
2F

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
